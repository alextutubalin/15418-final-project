#include <iostream>
#include <fstream>
#include <cmath>
#include "simulator.h"

#define ENABLE_SIMD 1

#if ENABLE_SIMD
#include <smmintrin.h>
#endif

inline bool isWithin(float obs_x, float obs_y, float* corners_x, float* corners_y) {
    for(size_t i=0; i<4; i++) {
        int i2 = (i+1) % 4;
        float A = corners_y[i] - corners_y[i2];
        float B = corners_x[i2] - corners_x[i];
        float C = -B*corners_y[i] - A*corners_x[i];
        float D = A*obs_x + B*obs_y + C;
        if (D < 0) { 
            return false;
        }
    }
    return true;
}

#if ENABLE_SIMD
inline bool isWithin_vector(float obs_x, float obs_y, __m128 corners_x, __m128 corners_y,
                            __m128 corners_x2, __m128 corners_y2) {
    float zero = 0;
    __m128 vec0 = _mm_load_ss(&zero);
    __m128 obX = _mm_load1_ps(&obs_x);
    __m128 obY = _mm_load1_ps(&obs_y);
    __m128 A = _mm_sub_ps(corners_y, corners_y2);
    __m128 B = _mm_sub_ps(corners_x2, corners_x);
    __m128 D1 = _mm_add_ps(_mm_mul_ps(A, obX), _mm_mul_ps(B, obY));
    __m128 D2 = _mm_sub_ps(D1, _mm_mul_ps(B, corners_y));
    __m128 D3 = _mm_sub_ps(D2, _mm_mul_ps(A, corners_x));
    __m128 lt0 = _mm_cmplt_ps(D3, vec0);
    return (_mm_movemask_ps(lt0) == 0);
}
#endif

bool obsFree_seq(float* corners_x, float* corners_y, 
                float* obstacles_x, float* obstacles_y, int numobs) {
    for(int i=0; i<numobs; i++) {
        if (isWithin(obstacles_x[i], obstacles_y[i], corners_x, corners_y)) {
            return false;
        }
    }
    return true;
}

#if ENABLE_SIMD
bool obsFree_vector(float* corners_x, float* corners_y, 
                float* obstacles_x, float* obstacles_y, int numobs) {
    float corners_x2[4];
    float corners_y2[4];
    for(int i=0; i<4; i++) {
        corners_x2[i] = corners_x[(i+1)&3];
        corners_y2[i] = corners_y[(i+1)&3];
    }
    __m128 corners_x_vec = _mm_load_ps(corners_x);
    __m128 corners_y_vec = _mm_load_ps(corners_y);
    __m128 corners_x2_vec = _mm_load_ps(corners_x2);
    __m128 corners_y2_vec = _mm_load_ps(corners_y2);
    for(int i=0; i<numobs; i++) {
        if (isWithin_vector(obstacles_x[i], obstacles_y[i], corners_x_vec, corners_y_vec, 
            corners_x2_vec, corners_y2_vec)) {
            return false;
        }
    }
    return true;
}
#endif

Simulator::Simulator() {
}

Simulator::~Simulator() {
}

void Simulator::loadMap(const char* filename) {
    std::ifstream fin;
    
    fin.open(filename);
    fin >> car.x >> car.y >> car.heading;
    fin >> target.x >> target.y >> target.heading;
    while(1) {
        float obs_x, obs_y;
        fin >> obs_x >> obs_y;
        if (obs_x < 0.0 || obs_y < 0.0) {
            break;
        }
        obstacles_x.push_back(obs_x);
        obstacles_y.push_back(obs_y);
        /*
        for(int j=0; j<50; j++) {
            obstacles_x.push_back(obs_x + 0.2 * drand48() - 0.1);
            obstacles_y.push_back(obs_y + 0.2 * drand48() - 0.1);
        }
        */
    }
    fin.close();
}

bool Simulator::obsFree(const Point3D& point) {
    return obsFree(Car(point.x, point.y, point.heading));
}

bool Simulator::obsFree(const Car& projected_car) {
    float corners_x[4];
    float corners_y[4];
    projected_car.getCorners(corners_x, corners_y);
    for(size_t j=0; j<4; j++) {
        if (corners_x[j] < 0 || corners_x[j] > WORLD_WIDTH
         || corners_y[j] < 0 || corners_y[j] > WORLD_HEIGHT) {
            return false;
        }
    }

    #if ENABLE_SIMD
    return obsFree_vector(corners_x, corners_y, (float*)(obstacles_x.data()),
        (float*)(obstacles_y.data()), obstacles_x.size());
    #else
    return obsFree_seq(corners_x, corners_y, (float*)(obstacles_x.data()),
        (float*)(obstacles_y.data()), obstacles_x.size());
    #endif
}
