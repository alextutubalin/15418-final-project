#ifndef _PATHPLAN_H_
#define _PATHPLAN_H_

#include "common.h"
#include "simulator.h"
#include "kdtree.h"

const float MIN_STEP = 0.5;

class PathPlanner {
    public:
        KDNode* tree;
        Simulator* sim;
        
        PathPlanner(Simulator* _sim): sim(_sim) {}
        ~PathPlanner() {
            if (tree != NULL) {
                delete tree;
            }
        }
        void plan();
};

#endif
